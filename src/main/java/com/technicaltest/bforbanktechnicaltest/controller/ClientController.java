package com.technicaltest.bforbanktechnicaltest.controller;

import com.technicaltest.bforbanktechnicaltest.api.ClientApi;
import com.technicaltest.bforbanktechnicaltest.exceptions.InconsistentDataException;
import com.technicaltest.bforbanktechnicaltest.model.ClientDTO;
import com.technicaltest.bforbanktechnicaltest.domain.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@Slf4j
public class ClientController implements ClientApi {

    @Autowired
    ClientService clientService;

    @Override
    public ResponseEntity<ClientDTO> createClient(@RequestBody ClientDTO clientDTO) {
        log.info("createClient"+clientDTO.toString());
        this.validateClient(clientDTO);
        return new ResponseEntity<>(this.clientService.createClient(clientDTO), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ClientDTO> getclientById(Integer  id) {
        log.info("getclientById"+id);
        return new ResponseEntity<>(this.clientService.getClientByClientId(id), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ClientDTO> updateclient(@RequestBody ClientDTO clientDTO) {
        log.info("updateclient"+clientDTO.toString());
        this.validateClient(clientDTO);
        return new ResponseEntity<>(this.clientService.updateClient(clientDTO), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> deleteclientById(Integer id) {
        log.info("deleteclientById"+id);
        this.clientService.deleteClient(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void validateClient(ClientDTO clientDTO) {

        Pattern pattern = Pattern.compile("^(.+)@(.+)$");
        Pattern patternPhone = Pattern.compile("^(0|\\+33)[1-9]([-. ]?[0-9]{2}){4}$");

        Matcher matcher = pattern.matcher(clientDTO.getEmail());
        Matcher matcherPhone = patternPhone.matcher(clientDTO.getPhone());
        if (!matcher.matches()) {
            throw new InconsistentDataException("Email is not valid");
        }
        if (!matcherPhone.matches()) {
            throw new InconsistentDataException("Phone is not valid");
        }
        if (null == clientDTO.getFirstname() || clientDTO.getFirstname().length() < 2) {
            throw new InconsistentDataException("Firstname is not valid");
        }
        if (null == clientDTO.getLastname() || clientDTO.getLastname().length() < 2) {
            throw new InconsistentDataException("Lastname is not valid");
        }
    }

}


