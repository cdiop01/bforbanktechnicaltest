package com.technicaltest.bforbanktechnicaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BforbanktechnicaltestApplication {

    public static void main(String[] args) {
        SpringApplication.run(BforbanktechnicaltestApplication.class, args);
    }


}
