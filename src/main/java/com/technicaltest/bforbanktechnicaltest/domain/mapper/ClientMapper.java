package com.technicaltest.bforbanktechnicaltest.domain.mapper;

import com.technicaltest.bforbanktechnicaltest.infrastructure.dao.ClientDAO;
import com.technicaltest.bforbanktechnicaltest.model.ClientDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    ClientDAO clientDTOToClientDAO(ClientDTO clientDTO);

    ClientDTO clientDAOToClientDTO(ClientDAO clientDAO);

    ClientDTO clientJsonToClientDTO(String clientJson);

}
