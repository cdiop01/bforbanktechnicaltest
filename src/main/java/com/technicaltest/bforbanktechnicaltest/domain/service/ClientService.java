package com.technicaltest.bforbanktechnicaltest.domain.service;

import com.technicaltest.bforbanktechnicaltest.domain.mapper.ClientMapper;
import com.technicaltest.bforbanktechnicaltest.exceptions.ClientNotFoundException;
import com.technicaltest.bforbanktechnicaltest.exceptions.InconsistentDataException;
import com.technicaltest.bforbanktechnicaltest.infrastructure.dao.ClientDAO;
import com.technicaltest.bforbanktechnicaltest.infrastructure.repository.ClientRepository;
import com.technicaltest.bforbanktechnicaltest.model.ClientDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ClientService {

    @Autowired
    ClientRepository clientRepository;

    @Autowired
    ClientMapper mapper;

    public ClientDTO createClient(ClientDTO clientToCreate) {
        ClientDAO storedClient = this.clientRepository.save(this.mapper.clientDTOToClientDAO(clientToCreate));
        log.info("Client created with id: " + storedClient.getIdClient());
        return this.mapper.clientDAOToClientDTO(storedClient);
    }

    public ClientDTO getClientByClientId(Integer idClient){
        ClientDAO clientDAO = this.findClientByIdClient(idClient);
        log.info("Client found with id: " + clientDAO.getIdClient());
        return  this.mapper.clientDAOToClientDTO(clientDAO);
    }


    public ClientDTO updateClient(ClientDTO clientToUpdate) {
        ClientDAO clientDAO = null;
        if (null == clientToUpdate.getIdClient()) {
            throw new InconsistentDataException("id user is null");
        }
        else {
            clientDAO = this.findClientByIdClient(clientToUpdate.getIdClient());
            clientDAO.setFirstname(clientToUpdate.getFirstname());
            clientDAO.setLastname(clientToUpdate.getLastname());
            clientDAO.setEmail(clientToUpdate.getEmail());
            clientDAO.setPhone(clientToUpdate.getPhone());
        }
        ClientDAO storedClient = this.clientRepository.save(clientDAO);
        log.info("Client  with id: " + storedClient.getIdClient()+" is updated ");
        return this.mapper.clientDAOToClientDTO(storedClient);
    }

    public void deleteClient(Integer clientId) {
        ClientDAO clientDAO = this.findClientByIdClient(clientId);
        log.info("Client  with id: " + clientDAO.getIdClient()+" is found  ");
        this.clientRepository.delete(clientDAO);
    }

    private ClientDAO findClientByIdClient(Integer idClient) {

        return clientRepository.findByIdClient(idClient).orElseThrow(() -> new ClientNotFoundException("Client not found"));
    }

}
