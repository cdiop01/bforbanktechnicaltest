package com.technicaltest.bforbanktechnicaltest.infrastructure.repository;

import com.technicaltest.bforbanktechnicaltest.infrastructure.dao.ClientDAO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ClientRepository extends CrudRepository<ClientDAO, Integer> {
    Optional<ClientDAO> findByIdClient(Integer idClient);
}
