package com.technicaltest.bforbanktechnicaltest.infrastructure.dao;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Table(name = "client")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_client")
    private Integer idClient;

    private String firstname;

    private String lastname;

    private String email;

    private String phone;

}
