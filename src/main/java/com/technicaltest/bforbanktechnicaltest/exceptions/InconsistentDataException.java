package com.technicaltest.bforbanktechnicaltest.exceptions;

public class InconsistentDataException extends RuntimeException {
    public InconsistentDataException(String message) {
        super(message);
    }
}

