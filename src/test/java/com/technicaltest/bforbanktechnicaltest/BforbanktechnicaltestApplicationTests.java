package com.technicaltest.bforbanktechnicaltest;

import com.technicaltest.bforbanktechnicaltest.domain.mapper.ClientMapper;
import com.technicaltest.bforbanktechnicaltest.exceptions.ClientNotFoundException;
import com.technicaltest.bforbanktechnicaltest.infrastructure.dao.ClientDAO;
import com.technicaltest.bforbanktechnicaltest.model.ClientDTO;
import com.technicaltest.bforbanktechnicaltest.util.AbstractIntegrationTesting;
import com.technicaltest.bforbanktechnicaltest.util.FileUtil;
import org.junit.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.jdbc.Sql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Sql(scripts = "classpath:sql/data.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
public class BforbanktechnicaltestApplicationTests extends AbstractIntegrationTesting {

    @Autowired
    TestRestTemplate restTemplate;

    @Spy
    ClientMapper clientMapper = Mappers.getMapper(ClientMapper.class);


    @Test
    public void shouldCreateClient() {
        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setEmail("cdiop288@gmail.com");
        clientDTO.setFirstname("Cheikh");
        clientDTO.setLastname("DIOP");
        clientDTO.setPhone("+33695933647");

        var response = restTemplate.exchange("http://localhost:" + port + "/api/client", HttpMethod.POST, new HttpEntity<>(clientDTO), ClientDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(clientDTO.getFirstname(), response.getBody().getFirstname());
        assertEquals(clientDTO.getLastname(), response.getBody().getLastname());
        assertEquals(clientDTO.getEmail(), response.getBody().getEmail());
        assertEquals(clientDTO.getPhone(), response.getBody().getPhone());

    }

    @Test
    public void shouldNotCreateClientBecauseOfMalformedInput() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        String clientDTO = FileUtil.readResourceFileToString("json/ClientSample.json");
        HttpEntity<String> request = new HttpEntity<>(clientDTO, headers);

        var response = restTemplate.exchange("http://localhost:" + port + "/api/client", HttpMethod.POST, request, ClientDTO.class);

        assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode());

    }


    @Test
    public void shouldUpdateClient() {
        // Client with id 99 should exist because it is created in the data.sql file
        ClientDAO savedClient = this.clientRepository.findById(99).orElseThrow(() -> new ClientNotFoundException("Client not found"));

        var clientDTO = clientMapper.clientDAOToClientDTO(savedClient);

        clientDTO.setFirstname("Cheikh1");

        var response = restTemplate.exchange("http://localhost:" + port + "/api/client", HttpMethod.PUT, new HttpEntity<>(clientDTO), ClientDTO.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(clientDTO.getFirstname(), response.getBody().getFirstname());
        assertEquals(clientDTO.getLastname(), response.getBody().getLastname());
        assertEquals(clientDTO.getEmail(), response.getBody().getEmail());
        assertEquals(clientDTO.getPhone(), response.getBody().getPhone());

    }


    @Test
    public void shouldGetClientById() {

        // Client with id 99 should exist because it is created in the data.sql file
        ClientDAO createdClient = this.clientRepository.save(ClientDAO.builder()
                .email("cdiop@ciop.com").firstname("Cheikh").lastname("DIOP").phone("+33695933647").build());


        var response = restTemplate.exchange("http://localhost:" + port + "/api/client/" + createdClient.getIdClient(), HttpMethod.GET, null, ClientDTO.class);

        assertEquals(createdClient.getFirstname(), response.getBody().getFirstname());
        assertEquals(createdClient.getLastname(), response.getBody().getLastname());
        assertEquals(createdClient.getEmail(), response.getBody().getEmail());
        assertEquals(createdClient.getPhone(), response.getBody().getPhone());

    }


    @Test
    public void shouldDeleteClientById() {
        ClientDAO clientDAO = ClientDAO.builder()
                .email("cdiop@ciop.com").firstname("Cheikh").lastname("DIOP").phone("+33695933647").build();


        ClientDAO createdClient = this.clientRepository.save(clientDAO);


        var response = restTemplate.exchange("http://localhost:" + port + "/api/client/" + createdClient.getIdClient(), HttpMethod.DELETE, null, Void.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertThrows(ClientNotFoundException.class, () -> this.clientRepository.findById(createdClient.getIdClient()).orElseThrow(() -> new ClientNotFoundException("Client not found")));
    }

    @Test
    public void deleteShouldReturn404BecauseClientNotFound() {

        var response = restTemplate.exchange("http://localhost:" + port + "/api/client/" + 15, HttpMethod.DELETE, null, Void.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void getShouldReturn404BecauseClientNotFound() {

        var response = restTemplate.exchange("http://localhost:" + port + "/api/client/" + 15, HttpMethod.GET, null, Void.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void updateShouldReturn404BecauseClientNotFound() {

        ClientDTO clientDTO = new ClientDTO();
        clientDTO.setEmail("cdiop288@gmail.com");
        clientDTO.setFirstname("Cheikh");
        clientDTO.setLastname("DIOP");
        clientDTO.setPhone("+33695933647");
        clientDTO.setIdClient(15);
        var response = restTemplate.exchange("http://localhost:" + port + "/api/client", HttpMethod.PUT, new HttpEntity<>(clientDTO), ClientDTO.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }


}
