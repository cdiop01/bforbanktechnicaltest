package com.technicaltest.bforbanktechnicaltest.util;

import lombok.extern.slf4j.Slf4j;
import org.testcontainers.shaded.com.google.common.base.Charsets;
import org.testcontainers.shaded.com.google.common.io.Resources;

import java.net.URL;

@Slf4j
public class FileUtil {

    public static String readResourceFileToString(String filePath) {
        URL url = Resources.getResource(filePath);
        try {
            return Resources.toString(url, Charsets.UTF_8);
        } catch (Exception e) {
            log.error("Error while reading file", e);
            return null;
        }
    }
}
