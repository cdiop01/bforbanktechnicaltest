FROM maven:3.8.6-openjdk-18
EXPOSE 8080
ADD target/bforbanktechnicaltest-1.0.jar bforbank1.0.jar
ENTRYPOINT ["java","-jar","/bforbank1.0.jar"]
