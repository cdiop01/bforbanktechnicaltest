# BforBankTechnicalTest

## Comment lancer l'API en local ?

### 1 - Installer les prérequis

- Installer java 17 


- Installer maven 


- Installer Docker 


- Installer docker-compose 


### 2 - Récupérer le code source en local

- HTTPS:
  `git clone https://gitlab.com/cdiop01/bforbanktechnicaltest.git `
- 
### 3 - Lancer l'application

- se placer sur le répertoire du projet :
  `cd bforbanktechnicaltest/`


- builder l'application et lancer les tests :
  `mvn package`


- lancer l'api:
  `docker-compose.yml up`


- Pour accéder à la documentation de l'api aller sur :
  `http://localhost:8099/swagger-ui.html`
